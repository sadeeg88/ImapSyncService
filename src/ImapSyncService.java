//--------------------------------------------------------------------------------------------------------------------
//ImapSyncService
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------
import de.ImapSyncService.*;

public class ImapSyncService {

	public static void main(String[] args) {
		if(args.length == 2 && args[0].equalsIgnoreCase("sync"))
		{
			boolean fast = args[1].equals("fast");
			ImapSyncServiceWorker worker = new ImapSyncServiceWorker(fast);
			worker.startWorker();
		}
		else if(args.length == 1 && args[0].equalsIgnoreCase("analyse") )
		{
			ImapSyncServiceWorker worker = new ImapSyncServiceWorker(false);
			worker.startAn();
		}
		else if (args.length == 10 && args[0].equalsIgnoreCase("create"))
		{
			OrderGenerator order = new OrderGenerator();
		    try {
				order.makeOrder(args);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
			System.out.println("Parameter ung�ltig");
	}

}
