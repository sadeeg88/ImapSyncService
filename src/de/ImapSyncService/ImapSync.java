//--------------------------------------------------------------------------------------------------------------------
//ImapSyncService
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------
package de.ImapSyncService;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.GeneralSecurityException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.mail.util.SharedByteArrayInputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sun.mail.util.MailSSLSocketFactory;

public class ImapSync {
	
	private Session targetServer = null,
					sourceServer = null;
	private Store   targetMail = null,
					sourceMail = null;
	
	private long mailsToMigarte = 0;
	private long mailsMigrate = 0;
	private long mailsAreMigrated = 0;
	
	private boolean useFastCopy = true;
	
	
	private Folder  rootSource = null;
	ArrayList<String> FolderToMigration = new ArrayList<String>();
				
	
	private Calendar startTime = null;
	private SyncOrder order = null;
	private static final Logger logger = LogManager.getLogger(ImapSync.class);
	private static final Logger stateLogger = LogManager.getLogger("de.state");
	
	public ImapSync(SyncOrder order, boolean fast)
	{
		logger.info("Init Logger"+order.OrderName);
		this.order = order;
		this.useFastCopy = fast;
	}
	
	private void connect() throws MessagingException
	{
		logger.info("Connect");
		
	       //Get system properties
        
        Properties props = new Properties();
        //Mail-Server properties: Session verlangt die Informationen �ber Host, User, Passwd etc.
        MailSSLSocketFactory socketFactory;
		try {
			socketFactory = new MailSSLSocketFactory();
			socketFactory.setTrustAllHosts(true);
			props.put("mail.imaps.ssl.socketFactory", socketFactory);
			props.put("mail.imap.partialfetch", "false");
		} catch (GeneralSecurityException e) {
			logger.error("Connection Error",e);
		}
        
        
        
        //Session: steht f�r die Verbindung mit dem Mail-Server      
        sourceServer = Session.getDefaultInstance(props);
        sourceServer.setDebug(false);
        
        targetServer = Session.getDefaultInstance(props);
        targetServer.setDebug(false);

     
       connectSource();
       connectTarget();
        

		
	}
	
	
	public void init() throws MessagingException
	{
			//Folder mig = targetMail.getFolder("Migration");
			//if(!mig.exists())
			//	mig.create(3);
			//rootTarget = mig;
			rootSource = sourceMail.getDefaultFolder();
	
	}
	
	public void findFolderToMigrate() throws MessagingException
	{
		Folder [] folders = rootSource.list("*");
		for (Folder folder : folders) {
			FolderToMigration.add(folder.getFullName());
			logger.info("Find Folder:"+folder.getFullName());
			try
			{
			mailsToMigarte+= folder.getMessageCount();
			}catch(Exception ex)
			{
				logger.info("Fehler bei getCount");
			}
		
		}
		
	}
	
	public void analyseSync()
	{
		try {
			connect();
			init();
			findFolderToMigrate();
			startTime = Calendar.getInstance();
			System.out.println(mailsToMigarte);
			
			
		} catch (Exception e) {
			logger.error("ERROR startSync:",e);
		}
	}
	
	public void startSync()
	{
		try {
			connect();
			init();
			startTime = Calendar.getInstance();
			findFolderToMigrate();
			FolderSync();
			
			
		} catch (Exception e) {
			logger.error("ERROR startSync:",e);
		}
		
        saveOrder();
        String logZusammenfassung = "Zusammenfasssung: ";
        logZusammenfassung += " Order:"+order.OrderName+" ";
        logZusammenfassung += "ErmittelteMails: "+mailsToMigarte+" "+
        					"Kopierte Mails:"+order.uuis.size();
        logger.info(logZusammenfassung);
	}

	private void saveOrder() {
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(order.OrderName+".syncOrder");

        
        JAXBContext jaxbContext = JAXBContext.newInstance(SyncOrder.class);
        Marshaller m = jaxbContext.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // To format XML
        m.marshal(order, fileOut);

        fileOut.flush();
        fileOut.close();
        logger.info("Save Order to:"+order.OrderName+".syncOrder");
		} catch (Exception e) {
			logger.error("ERROR startSync SAVE:",e);
		}
	}
	
	
	private void FolderSync() throws MessagingException
	{
		Folder firsttarget = targetMail.getFolder("Migration2017");
		if(!firsttarget.exists())
			firsttarget.create(Folder.HOLDS_FOLDERS+Folder.HOLDS_MESSAGES);
		
		for (String strFolder : FolderToMigration) {
			logger.debug("Sync Folder:"+strFolder);
			
			Boolean retry = false;
			int trycount = 0;
			do{
			try {
				retry = false;
				Folder source = sourceMail.getFolder(strFolder);
				String Folder2 = strFolder.replace(".", "-"); //Maskieren von Punkten, da es Probleme gab
				Folder target = firsttarget.getFolder(Folder2);
			if(!target.exists())
					target.create(Folder.HOLDS_FOLDERS+Folder.HOLDS_MESSAGES);
			if(target.getType() != Folder.HOLDS_FOLDERS+Folder.HOLDS_MESSAGES)
					target.create(Folder.HOLDS_FOLDERS+Folder.HOLDS_MESSAGES);
			SyncMessages(source, target);
			} catch (Exception ex) {
				logger.error(" Exception Sync Folder:",ex);
				retry = true;
				trycount++;
				if(!sourceMail.isConnected())
					connectSource();
				if(!targetMail.isConnected())
					connectTarget();
			}
			}
			while(retry && trycount < 3);
		}
		
	}
	
	public void SyncMessages(Folder source, Folder target) throws Exception
	{
		source.open(Folder.READ_ONLY);
		Exception error = null;
		int nr = 0;
		UIDFolder sourceUUID = (UIDFolder) source;
		Message[] a = source.getMessages();
		logger.debug("Nachrichten Anzahl:"+a.length);
		for (Message message : a) {
			nr++;
			long uuid = sourceUUID.getUID(message);	
			if(!order.uuis.contains(uuid))
			{
				logger.debug("Syncronisiere Nachricht UUID:"+uuid+" Nr."+nr);
				boolean OK = true;
				int count = 0;
				do{
				try
				{
					OK= true;
					count++;	
					copyMessage(message, source, target);
					order.uuis.add(uuid);
				}catch(javax.mail.MessagingException|IOException ex)
				{
					OK = false;
				}
				}while(!OK && count <= 3  );
				if(!OK)
				{
					logger.error("Nicht behebarer Fehler UUID:"+uuid,error);
				}
				else
				{
					logger.info("Copy Erfolgreich!");
					mailsMigrate++;
				}
					
			}
			else
			{
				logger.debug("Nachricht wurde schon kopiert!");
				mailsAreMigrated++;
			}
		
				
			stateLoggs();
			saveOrder();
			
		}
		
	}
	
	private void stateLoggs()
	{
		double persentFull =(100.0 / mailsToMigarte * (mailsMigrate+mailsAreMigrated)) ;
		double persentMigate =( 100.0 / (mailsToMigarte-mailsAreMigrated) * mailsMigrate) ;
		stateLogger.info("Gesammte Mails:"+mailsToMigarte+" --- Migriert:"+mailsMigrate+" --- Waren Migriert:"+mailsAreMigrated);
		stateLogger.info("Gesammt:"+persentFull+"%  ---  Migrierte:"+persentMigate+"%");
		if(useFastCopy)
			stateLogger.info("fast");
		
	}
	
	public void copyMessage(Message msg, Folder source, Folder target) throws MessagingException, IOException
	{
		Exception ex1 = null;
		try
		{
			if(useFastCopy)
			{
				copyMessageB(msg, source, target);
			}
			else
			{
				copyMessageA(msg, source, target);
			}
		}catch(MessagingException|IOException ex)
		{
			try
			{
			if(!useFastCopy)
			{
				copyMessageB(msg, source, target);
			}
			else
			{
				copyMessageA(msg, source, target);
			}
			}catch(MessagingException|IOException ex2)
			{
				throw ex;
			}
			
		}
		
	}
	
	public void copyMessageA(Message msg, Folder source, Folder target)throws MessagingException
	{
		if(!target.isOpen())
		target.open(Folder.READ_WRITE);
		Message[] msgs = new Message[1];
		msgs[0] = msg;
		source.copyMessages(msgs, target);
	}
	
	public void copyMessageB(Message msg, Folder source, Folder target)throws MessagingException, IOException
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    msg.writeTo(bos);
	    bos.close();
	    SharedByteArrayInputStream bis =
			    new SharedByteArrayInputStream(bos.toByteArray());
	    MimeMessage cmsg = new MimeMessage(targetServer, bis);
		Message[] msgs = new Message[1];
		msgs[0] = cmsg;
	    bis.close();
	    target.appendMessages(msgs);
	}
	
	public void connectSource() throws MessagingException
	{
		logger.info("ConnectSource");
        sourceMail = sourceServer.getStore("imap");
        sourceMail.connect(order.SourceServer,order.SourceUser,order.SourcePassword);
        logger.info("ConnectSource erfolgreich");
	}
	
	public void connectTarget() throws MessagingException
	{
		 logger.info("ConnectTarget");
		 targetMail = targetServer.getStore("imaps");
	     targetMail.connect(order.TargetServer,order.TargetUser,order.TargetPassword);
	     logger.info("ConnectTarget erfolgreich");
		
	}
	
	

}
