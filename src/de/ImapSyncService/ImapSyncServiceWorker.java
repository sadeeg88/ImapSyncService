//--------------------------------------------------------------------------------------------------------------------
//ImapSyncService
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------
package de.ImapSyncService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ImapSyncServiceWorker {
	private static final Logger logger = LogManager.getLogger(ImapSyncServiceWorker.class);
	
	boolean fast = false;
	
	public ImapSyncServiceWorker(boolean fast)
	{
		this.fast = fast;
		logger.info("Start Imapworker");
	}
	
	
	public void startWorker()
	{
		ArrayList<SyncOrder> orders = readOrders();
		logger.info("Auftr�ge"+orders.size());
		for (SyncOrder syncOrder : orders) {
			ImapSync sync = new ImapSync(syncOrder,fast);
			sync.startSync();
		}
	
		
	}
	
	public void startAn()
	{
		ArrayList<SyncOrder> orders = readOrders();
		for (SyncOrder syncOrder : orders) {
			ImapSync sync = new ImapSync(syncOrder, fast);
			sync.analyseSync();
		}
	
		
	}
	
	public ArrayList<SyncOrder> readOrders()
	{
		ArrayList<SyncOrder> orders = new ArrayList<SyncOrder>();
		File dir = new File(".");
		File[] files = dir.listFiles();
		for(int x = 0 ; x < files.length; x++)
		{
			if(files[x].isFile() && files[x].canRead())
			{
					if(files[x].getName().endsWith(".syncOrder"))
					{
						String name = files[x].getAbsolutePath();
						try
					      {
					         FileInputStream fileIn = new FileInputStream(name);
					        // ObjectInputStream in = new ObjectInputStream(fileIn);
					 		JAXBContext jaxbContext = JAXBContext.newInstance(SyncOrder.class);
							Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
							SyncOrder e  = (SyncOrder) jaxbUnmarshaller.unmarshal(fileIn);	  
					         orders.add(e);
					         //in.close();
					         fileIn.close();
					      }catch(IOException i)
					      {
					    	 logger.error("Ex",i);
					      }
						catch(JAXBException c)
					     {
							logger.error("Ex",c);
					     }
						
					}
			
			}
			
		}
		
		return orders;
	}
	
	

}
